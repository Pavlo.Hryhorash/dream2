package Juice.presenter;

import Juice.model.Juice;
import Juice.model.type.JuiceType;
import Juice.view.JuiceView;

import java.util.ArrayList;
import java.util.List;

public class ViewPresenter {

    private JuiceView juiceView;

    private List<Juice> listOfJuice = new ArrayList<>();

    public ViewPresenter(JuiceView juiceView) {
        this.juiceView = juiceView;
        setUpDatabase();
        startFabricWork();
    }

    private void setUpDatabase() {
        listOfJuice.add(new Juice(1, "Sadochok", 1, JuiceType.APPLE));
        listOfJuice.add(new Juice(2, "Sandora", 2, JuiceType.BANANA));
        listOfJuice.add(new Juice(3, "Nash", 2, JuiceType.ORANGE));
        listOfJuice.add(new Juice(4, "Jaffa", 1, JuiceType.APPLE));
    }

    private void startFabricWork() {
        juiceView.showStartingFabricWork();
        boolean isFabricWorking = true;
        do {
            int variationOfWork = juiceView.getVariationsOfFabricWork();
            switch (variationOfWork){
                case 1:
                    showAllJuices();
                    break;
                case 2:
                    showJuiceByParameters();
                    break;
                case 3:
                    juiceView.showData("Enter pls id ");
                    deleteJuiceById(Integer.parseInt(juiceView.getDataFromConsole()));
                    break;
                case 4:
                    createNewJuice();
                    break;
                case 5:
                    isFabricWorking = false;
                    break;
            }

        } while (isFabricWorking);
    }

    public void createNewJuice() {
        Juice newJuice = new Juice();

        juiceView.showData("Enter Juice name");
        String juiceName = juiceView.getDataFromConsole();
        newJuice.setName(juiceName);
        juiceView.showData("Enter id");
        int juiceId = Integer.parseInt(juiceView.getDataFromConsole());
        newJuice.setId(juiceId);
        juiceView.showData("Enter volumes");
        int juiceVolumes = Integer.parseInt(juiceView.getDataFromConsole());
        newJuice.setVolumes(juiceVolumes);
        juiceView.showData("Enter juice type: 1 if Apple, 2 if Banana, 3 if Orange");
        int juiceType = Integer.parseInt(juiceView.getDataFromConsole());
        newJuice.setJuiceType(applyJuiceType(juiceType));

        listOfJuice.add(newJuice);


    }

    public void showJuiceByParameters() {
        Juice juiceExampleObject = new Juice();

        juiceView.showData("Enter Juice name");
        String juiceName = juiceView.getDataFromConsole();
        juiceExampleObject.setName(juiceName);
        juiceView.showData("Enter id");
        int juiceId = Integer.parseInt(juiceView.getDataFromConsole());
        juiceExampleObject.setId(juiceId);
        juiceView.showData("Enter volumes");
        int juiceVolumes = Integer.parseInt(juiceView.getDataFromConsole());
        juiceExampleObject.setVolumes(juiceVolumes);
        juiceView.showData("Enter juice type: 1 if Apple, 2 if Banana, 3 if Orange");
        int juiceType = Integer.parseInt(juiceView.getDataFromConsole());
        juiceExampleObject.setJuiceType(applyJuiceType(juiceType));

        for (Juice item : listOfJuice) {
            if (item.getId() == juiceExampleObject.getId()) {
                showJuice(item);
            } else if (item.getName().equals(juiceExampleObject.getName()))  {
                showJuice(item);
            } else if (item.getVolumes() == juiceExampleObject.getVolumes()) {
                showJuice(item);
            } else if (item.getJuiceType() == juiceExampleObject.getJuiceType()) {
                showJuice(item);
            }
        }

    }

    public void deleteJuiceById(int id) {

        for (Juice item : listOfJuice) {
            if (item.getId() == id) {

                listOfJuice.remove(item);
                juiceView.showData("Item "+ item.getId() + " has been deleted");
            }
        }
    }


    public void showAllJuices() {
        for (Juice item : listOfJuice) {
            showJuice(item);
        }

    }

    private void showJuice(Juice juice) {
        juiceView.showSeparator();
        juiceView.showData("Id -> " + juice.getId());
        juiceView.showData("Name -> " + juice.getName());
        juiceView.showData("Volumes -> " + juice.getVolumes());
        juiceView.showData("Type of juice ->" + juice.getJuiceType());
        juiceView.showSeparator();
    }


    private JuiceType applyJuiceType(int index) {
        switch (index) {
            case 1:
                return JuiceType.APPLE;
            case 2:
                return JuiceType.BANANA;
            case 3:
                return JuiceType.ORANGE;
            default:
                return null;
        }
    }
}
