package Juice.model.type;

import Juice.model.Juice;

public enum JuiceType {
    ORANGE,
    APPLE,
    BANANA
}
