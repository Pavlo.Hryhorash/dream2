package Juice.view;

public interface JuiceView {
    void showData(String message);
    void showSeparator();
    void showStartingFabricWork();
    int getVariationsOfFabricWork();

    String getDataFromConsole();
}
