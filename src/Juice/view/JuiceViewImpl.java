package Juice.view;

import Juice.presenter.ViewPresenter;

import java.util.Scanner;

public class JuiceViewImpl implements JuiceView {

    public JuiceViewImpl() {
        new ViewPresenter(this);
    }

    @Override
    public void showData(String message) {
        System.out.println(message);
    }

    @Override
    public void showSeparator() {
        System.out.println("-----------------------------------------");
    }

    @Override
    public String getDataFromConsole() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    @Override
    public void showStartingFabricWork() {
        System.out.println("Welcome to fabric");
    }

    @Override
    public int getVariationsOfFabricWork() {
        System.out.println("Choose variant of function");
        System.out.println("1 - show all juices\n" +
                "2 - show juice by parametres\n" +
                "3 - delete juice by id\n" +
                "4 - create juice\n" +
                "5 - end fabric work");
        return new Scanner(System.in).nextInt();
    }
}
