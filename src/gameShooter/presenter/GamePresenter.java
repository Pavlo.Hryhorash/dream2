package gameShooter.presenter;

import gameShooter.model.Skin;
import gameShooter.model.Weapon;
import gameShooter.model.type.SkinRole;
import gameShooter.model.type.WeaponType;
import gameShooter.view.GameView;

public class GamePresenter {

    private final int SKIN_HEALTH = 100;

    private GameView gameView;
    private Weapon[] weapons = new Weapon[4];

    private Skin user;
    private Skin robot;

    public GamePresenter(GameView gameView) {
        this.gameView = gameView;
    }

    public void startGame() {
        createWeapon();
        createUser();
        createBot();


        int whoIsShooting = (int) (Math.random() * 3);

        boolean isGamePlaying = true;
        do {
            if (whoIsShooting >= 1) {
                skinShoot(user, robot);
                whoIsShooting = 0;
                gameView.setSeparator();
                skinSate(robot);
                isGamePlaying = isSkinHasHealth(robot);
            } else {
                skinShoot(robot, user);
                whoIsShooting = 1;
                gameView.setSeparator();
                skinSate(user);
                isGamePlaying = isSkinHasHealth(user);
            }


        } while (isGamePlaying);

        if (user.getHealth() > robot.getHealth()) {
            gameView.outputText(user.getName() + " is winner");
        } else {
            gameView.outputText(robot.getName() + " is winner");
        }
    }

    private void createUser() {
        gameView.setSeparator();
        gameView.outputText("Set user name:");
        String userName = gameView.getDataFromView();
        gameView.outputText("Set Weapon:");
        int weaponIndex = Integer.parseInt(gameView.getDataFromView());
        Weapon weapon = chooseWeapon(weaponIndex);
        user = new Skin(userName, SKIN_HEALTH, weapon, SkinRole.GAMER);
        gameView.outputText("User -> " + userName + " created");

    }

    private void createBot() {
        gameView.setSeparator();
        Weapon weapon = chooseWeapon((int) (Math.random() * weapons.length - 1));
        robot = new Skin("Robot", SKIN_HEALTH, weapon, SkinRole.BOT);
        gameView.outputText("Bot -> " + robot.getName() + " created");
    }

    private void skinSate(Skin skin) {
        gameView.outputText("Skin -> " + skin.getName() + "\n" +
                "health -> " + user.getHealth() + "\n" +
                "role -> " + skin.getSkinRole());
    }

    private void skinShoot(Skin hunter, Skin prey) {
        gameView.setSeparator();
        gameView.outputText(hunter.getName() + " shooting " + prey.getName());
        if (isHunterHitPray(hunter)) {
            gameView.outputText(hunter.getName() + " damaged " + hunter.getWeapon().getDamage());
            prey.setHealth(prey.getHealth() - hunter.getWeapon().getDamage());
        } else {
            gameView.outputText(hunter.getName() + " miss shoot");
        }

    }

    private boolean isHunterHitPray(Skin hunter) {

        boolean result = false;
        int shoot;
        gameView.outputText(hunter.getName() + " make shoot ");
        if (hunter.getSkinRole() == SkinRole.GAMER) {
            shoot = Integer.parseInt(gameView.getDataFromView());
        } else {
            shoot = (int) (Math.random() * 14);
        }

        gameView.outputText("shoot is -> " + shoot);
        if (shoot > 4 && shoot < 10) {
            result = true;
        }
        return result;
    }

    private void createWeapon() {
        weapons[0] = new Weapon("Katana", 30, WeaponType.KNIFE);
        weapons[1] = new Weapon("AK-47", 40, WeaponType.GUN);
        weapons[2] = new Weapon("M16", 42, WeaponType.GUN);
        weapons[3] = new Weapon("Knife", 10, WeaponType.KNIFE);
    }

    private Weapon chooseWeapon(int weaponIndex) {
        if (weaponIndex > weapons.length) {
            return weapons[weapons.length - 1];
        }
        return weapons[weaponIndex];
    }

    private boolean isSkinHasHealth(Skin skin) {
        return skin.getHealth() > 0;
    }

}

