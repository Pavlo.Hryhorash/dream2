package gameShooter.view;

public interface GameView {
    void outputText(String outputText);
    String getDataFromView();
    void setSeparator();
}
