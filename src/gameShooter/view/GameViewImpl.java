package gameShooter.view;

import gameShooter.presenter.GamePresenter;

import java.util.Scanner;

public class GameViewImpl implements GameView {

    public GameViewImpl() {
        new GamePresenter(this).startGame();
    }

    @Override
    public void outputText(String outputText) {
        System.out.println(outputText);
    }

    @Override
    public String getDataFromView() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    @Override
    public void setSeparator() {
        System.out.println("=====================================");
    }
}
