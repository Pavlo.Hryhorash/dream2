package gameShooter.model;

import gameShooter.model.type.WeaponType;

public class Weapon {

    private String name;

    private int damage;

    private WeaponType weaponType;

    public Weapon() {
    }

    public Weapon(String name, int damage, WeaponType weaponType) {
        this.name = name;
        this.damage = damage;
        this.weaponType = weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }
}
