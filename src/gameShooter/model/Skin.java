package gameShooter.model;

import gameShooter.model.type.SkinRole;

public class Skin {

    private String name;

    private int health;

    private Weapon weapon;

    private SkinRole SkinRole;

    public Skin() {
    }

    public Skin(String name, int health, Weapon weapon, gameShooter.model.type.SkinRole skinRole) {
        this.name = name;
        this.health = health;
        this.weapon = weapon;
        SkinRole = skinRole;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public gameShooter.model.type.SkinRole getSkinRole() {
        return SkinRole;
    }

    public void setSkinRole(gameShooter.model.type.SkinRole skinRole) {
        SkinRole = skinRole;
    }
}
