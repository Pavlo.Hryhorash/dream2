package gameShooter.model.type;

public enum WeaponType {
    KNIFE,
    GUN
}
