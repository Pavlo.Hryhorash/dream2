package exceptions2.model.type;

public enum TypeBook {
    NOVEL,
    POETRY
}
