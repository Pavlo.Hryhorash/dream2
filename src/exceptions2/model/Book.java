package exceptions2.model;

import exceptions2.model.type.TypeBook;

public class Book {
    private int id;
    private String title;
    private TypeBook TypeBook;

    public Book() {
    }


    public Book(int id, String title, exceptions2.model.type.TypeBook typeBook) {
        this.id = id;
        this.title = title;
        TypeBook = typeBook;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public exceptions2.model.type.TypeBook getTypeBook() {
        return TypeBook;
    }

    public void setTypeBook(exceptions2.model.type.TypeBook typeBook) {
        TypeBook = typeBook;
    }
}
