package exceptions2;

import exceptions2.model.User;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<User> userSet = new LinkedHashSet<>();
        userSet.add(new User(1, "Pavlo"));
        userSet.add(new User(2, "Tanya"));
        userSet.add(new User(3, "Misha"));
        userSet.add(new User(3, "Misha"));

        for (User user : userSet) {
            System.out.println(user.getId());
            System.out.println(user.getName());
            System.out.println(user.toString());
            System.out.println("-------------------------------------");
        }

//        List<String> namelist = new ArrayList<>();
//        namelist.add("10");
//        namelist.add("20");
//        namelist.add("30");
//        namelist.add("40");
//        namelist.add(null);
//        namelist.add("50");
//        namelist.add("60");
//
//        for (String item : namelist) {
//
//            try {
//                System.out.println(item.substring(1));
//            } catch (Throwable throwable) {
//                System.out.println(throwable.getMessage());
//            }
//        }


    }
}
