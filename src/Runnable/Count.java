package Runnable;

public class Count extends Thread {
    private int sleep;

    public Count(String name, int sleep){
        this.sleep = sleep;
        this.setName(name);
        this.start();
    }


    @Override
    public void run(){
        System.out.println(this.getName() + " started!");
        for (int i = 0; i <10; i++){
            try {
                System.out.println("Thread " + currentThread().getName() + ", count = " + i );
                Thread.sleep(sleep);
            } catch (InterruptedException e){
                System.out.println(currentThread().getName() + " interrupted!");
            }
        }
        System.out.println(this.getName() + " finished");
    }
}
