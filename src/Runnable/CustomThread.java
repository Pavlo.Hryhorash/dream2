package Runnable;

public class CustomThread extends Thread {
    private Clock clock;

    public CustomThread(Clock clock, String threadName) {
        this.clock = clock;
        this.setName(threadName);
        this.start();
    }

    @Override
    public void run() {
        System.out.println(this.getName() + " is started");
        clock.count(this.getName());
        System.out.println(this.getName() + " is finished");
    }
}
