package Runnable;

public class MultithreadingApp {
    public static void main(String[] args) {
        System.out.println("Main thread is started");
//        Counter counter1 = new Counter("Child 1", 400);
//        Counter counter2 = new Counter("Child 2", 700);
//        Counter counter3 = new Counter("Child 3", 1000);
//        Count count1 = new Count("Count 1", 400);
//
//        Count count2 = new Count("Count 2", 700);
//        Count count3 = new Count("Count 3", 1000);
//        count1.setPriority(Thread.MIN_PRIORITY);
//        count2.setPriority(Thread.MIN_PRIORITY);
//        count3.setPriority(Thread.MAX_PRIORITY);
//        try {
//            count1.join();
//        } catch (InterruptedException e){
//            e.printStackTrace();
//        }
        Clock clock = new Clock();
        CustomThread thread1 = new CustomThread(clock, "Thread1");
        CustomThread thread2 = new CustomThread(clock, "Thread2");


        System.out.println("Main thread is finished!");
    }
}
