package lesson8.animals;

public enum AnimalType {
    MAMMAL,
    PREDATOR,
    CARNIVORE
}
