package lesson8.animals;

public class Animal {
    protected String name;
    protected int countOfLegs;
    protected Skin skinType;
    protected Security securityType;
    protected AnimalType animalType;

    public Animal(String name, int countOfLegs, Skin skinType, Security securityType, AnimalType animalType) {
        this.name = name;
        this.countOfLegs = countOfLegs;
        this.skinType = skinType;
        this.securityType = securityType;
        this.animalType = animalType;
    }

    public void animalVoice(){

    }

    public String getName() {
        return name;
    }

    public int getCountOfLegs() {
        return countOfLegs;
    }

    public Skin getSkinType() {
        return skinType;
    }

    public Security getSecurityType() {
        return securityType;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }
}
