package lesson8.animals;

public class Eagle extends Animal {
    public Eagle(String name, int countOfLegs, Skin skinType, Security securityType, AnimalType animalType) {
        super(name, countOfLegs, skinType, securityType, animalType);
    }

    @Override
    public void animalVoice() {
        System.out.println("Eagle is screaming");
    }

    public void eatRabbits(){
        System.out.println("Eating rabbits");
    }
}
