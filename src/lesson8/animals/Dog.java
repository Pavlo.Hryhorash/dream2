package lesson8.animals;

public class Dog extends Animal {
    public Dog(String name, int countOfLegs, Skin skinType, Security securityType, AnimalType animalType) {
        super(name, countOfLegs, skinType, securityType, animalType);
    }

    @Override
    public void animalVoice() {
        System.out.println("Dog is barging!!!");
    }

    public void getShoes(){
        System.out.println("Dog get for us shoes");
    }
}
