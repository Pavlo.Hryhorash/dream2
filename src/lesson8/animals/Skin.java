package lesson8.animals;

public enum Skin {
    WOOL,
    SCALES,
    FEATHER
}
