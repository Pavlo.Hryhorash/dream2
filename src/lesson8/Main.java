package lesson8;

import lesson8.animals.*;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal("Animal", 4, Skin.SCALES, Security.HORNS, AnimalType.CARNIVORE );


        Dog dog = new Dog("Altay", 4, Skin.WOOL, Security.CLAWS, AnimalType.PREDATOR);


        Eagle eagle = new Eagle("Lola", 2, Skin.FEATHER, Security.CLAWS, AnimalType.PREDATOR);

        System.out.println(dog.getName());
        dog.animalVoice();
        dog.getShoes();

        System.out.println(eagle.getName());
        eagle.animalVoice();
        eagle.eatRabbits();

        Animal eagle2 = new Eagle("Vector", 2, Skin.FEATHER, Security.CLAWS, AnimalType.PREDATOR);

        ((Eagle) eagle2).eatRabbits();


    }
}
