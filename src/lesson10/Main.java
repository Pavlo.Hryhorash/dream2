package lesson10;

public class Main {
    public static void main(String[] args) {
        Math math = new MathImpl();

        System.out.println(math.plus(5, 7));

        String alfabet = "abcdefg";
        char[] charArray = {'a', 'b', 'c', 'd', 'e'};
        char symbol = '2';

        boolean isAlfabetContainCharacter = alfabet.contains("ce");
        System.out.println(isAlfabetContainCharacter);

        boolean isAlfabetEqualsWithString = alfabet.equals(" ");
        System.out.println(isAlfabetEqualsWithString);

        char[] alfabetCharArray = alfabet.toCharArray();
        for (int i = 0; i < alfabetCharArray.length; i++){
            System.out.println(alfabetCharArray[i]);
        }

        System.out.println(alfabet.replaceAll("a","b"));

        StringBuilder builder = new StringBuilder();
        builder.append("Hello");
        builder.append(" ");
        builder.append("World");
        String hello = builder.toString();
        System.out.println(hello);
        builder.reverse();
        System.out.println(builder);







    }
}

