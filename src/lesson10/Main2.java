package lesson10;

public class Main2 {
    public static void main(String[] args) {
        String string = "Hello World";
        System.out.println(string);
        String one = string.substring(2,5);
        System.out.println(one);
        String two = string.substring(6);
        System.out.println(two);
        int three = string.indexOf("W");
        System.out.println(three);
        Boolean four = string.equals("Bye");
        System.out.println(four);
        String five = string.replaceAll("Hello World","dleoW olleH");
        System.out.println(five);

        char[] six = string.toCharArray();
        //System.out.println(six[10]);

        for (int i = six.length-1; i > -1; i--){
           System.out.print(six[i]);

        }

        StringBuilder seven = new StringBuilder("Hello");
        seven.reverse();
        System.out.println("\n" + seven);


    }
}
