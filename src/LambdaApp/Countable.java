package LambdaApp;

public interface Countable {
    int count(int number);
}
