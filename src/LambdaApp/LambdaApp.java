package LambdaApp;

public class LambdaApp {
    public static void main(String[] args) {
        Countable countable1 = new CountableImpl();
        int sum1 = countable1.count(10);
        int sum2 = countable1.count(15);


        Countable countable2 = new Countable() {
            @Override
            public int count(int number) {
                int sum10 = 0;
                for (int i = 0; i < number; i++) {
                    sum10++;
                }
                return sum10;
            }
        };

        Countable countable3 = (n) -> {
            int sum20 = 0;
            for (int i = 0; i < n; i++) {
                sum20++;
            }
            return sum20;
        };
    }
}
