package firstsLessons;

import java.util.Scanner;

public class lesson7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter amount of rows");
        int rows = scanner.nextInt();
        System.out.println("enter amount of columns");
        int columns = scanner.nextInt();
        System.out.println("enter symbol");
        String symbol = scanner.next();
        String[][] array = new String[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                array[i][j] = symbol;
            }
        }
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {

                    System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

    }
}
