package firstsLessons;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        System.out.println("Enter any number:");
        number = scanner.nextInt();
        switch (number) {
            case 5:
                System.out.println("You won TV");
                break;
            case 10:
                System.out.println("you won phone");
                break;
            default:
                System.out.println("You dont win anything");
                break;

        }
        int result = minus(5, 2);
        System.out.println(result);
        int result2 = divide(8, 4);
        System.out.println(result2);
        System.out.println(ifAequalseB(true, true));
        System.out.println(ifAequalseB(true, false));
        System.out.println(ifCmoreThenX(5, 3));
        System.out.println(ifClessThenX(3, 5));
        System.out.println(ifCnerivnoX(5, 5));
        String nameUser1 = "Yra";
        int ageUser1 = 6;
        String nameUser2 = "Vladislav";
        int ageUser2 = 15;
        String nameUser3 = "Bronesvyatoslav";
        int ageUser3 = 30;
        System.out.println("If user1 has valid name\n" + checkIfNameIsValid(nameUser1));
        System.out.println("If user1 has rules to use this site\n" + ageRules(ageUser1));
        System.out.println("========================================================");
        System.out.println("If user2 has valid name\n" + checkIfNameIsValid(nameUser2));
        System.out.println("If user2 has rules to use this site\n" + ageRules(ageUser2));
        System.out.println("========================================================");
        System.out.println("If user3 has valid name\n" + checkIfNameIsValid(nameUser3));
        System.out.println("If user3 has rules to use this site\n" + ageRules(ageUser3));
        System.out.println("========================================================");
        String email = "123@mail.com";
        System.out.println(isEmailValid(email));
        plus(1, 3);
        showUserInfo();
        showUserInfo("pavlo", 30);
    }

    public static void showUserInfo() {
        System.out.println("test\npavlo\n30\nfootball");
        int a = 1;
        int b = 2;
        int c = 3;
        int d = 4;
        System.out.println(a + " " + b + " ");
        System.out.println(a + b - c * d / b);
        System.out.println(1 + 2 * 5);
        String Name = "Pavlo";
        int age = 30;
        int age2 = 40;
        String InTheFuture = "JavaDeveloper";
        System.out.println(Name);
        System.out.println(InTheFuture);
        System.out.println(age2 - age);
    }

    public static void showUserInfo(String name, int age) {
        System.out.println(name);
        System.out.println(age);
    }

    public static void plus(int a, int b) {
        int result = a + b;
        System.out.println(result);
    }

    public static int minus(int a, int b) {
        return a - b;
    }

    public static int divide(int a, int b) {
        return a / b;
    }

    private static boolean ifAequalseB(boolean a, boolean b) {
        return a == b;
    }

    private static boolean ifCmoreThenX(int c, int x) {
        return c > x;
    }

    private static boolean ifClessThenX(int c, int x) {
        return c < x;
    }

    private static boolean ifCnerivnoX(int c, int x) {
        return c != x;
    }

    private static boolean checkIfNameIsValid(String name) {
        boolean result = false;
        if (name.isEmpty()) {
            result = false;
        } else if (name.length() >= 6 && name.length() <= 12) {
            result = true;
        }
        return result;
    }

    private static boolean ageRules(int age) {
        return age >= 12 ? true : false;
    }

    private static boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        } else if (email.endsWith("ru")) {
            return false;
        } else {
            return true;
        }
    }
}

