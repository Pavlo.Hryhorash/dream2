package firstsLessons;

public class lesson6 {
    public static void main(String[] args) {
        final int SIZE_I = 3;
        final int SIZE_J = 3;

        int n = 12;
        String[][] christmasTree = new String[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && (j == 5 || j == 6)) {
                    christmasTree[i][j] = "*";
                } else if (i == 1 && (j >= 4 && j <= 7)) {
                    christmasTree[i][j] = "*";
                } else if (i == 2 && (j >= 3 && j <= 8)) {
                    christmasTree[i][j] = "*";
                } else if (i == 3 && (j >= 2 && j <= 9)) {
                    christmasTree[i][j] = "*";
                } else if (i == 4 && (j >= 1 && j <= 10)) {
                    christmasTree[i][j] = "*";
                } else if (i == 5 && (j >= 0 && j <= 11)) {
                    christmasTree[i][j] = "*";
                } else {
                    christmasTree[i][j] = ".";
                }
            }
        }
        int tempI = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == tempI) {
                    System.out.print(christmasTree[i][j]);
                } else {
                    tempI = i;
                    System.out.print("\n");
                    System.out.print(christmasTree[i][j]);
                }

            }
        }

        System.out.print("\n");
        String[][] matrixOfStrings = new String[SIZE_I][SIZE_J];
        for (int i = 0; i < SIZE_I; i++) {
            for (int j = 0; j < SIZE_J; j++) {
                int randomNumber = (int) (Math.random() * 9);
                matrixOfStrings[i][j] = Integer.toBinaryString(randomNumber) + " is binary of " + randomNumber;
            }
        }
        for (int i = 0; i < SIZE_I; i++) {
            for (int j = 0; j < SIZE_J; j++) {
                System.out.println(matrixOfStrings[i][j]);
            }
        }


        int[][] matrix = new int[SIZE_I][SIZE_J];
        for (int i = 0; i < SIZE_I; i++) {
            for (int j = 0; j < SIZE_J; j++) {
                matrix[i][j] = (int) (Math.random() * 15);
            }
        }
        for (int i = 0; i < SIZE_I; i++) {
            for (int j = 0; j < SIZE_J; j++) {
                if (matrix[i][j] > 4 && matrix[i][j] < 7) {
                    System.out.println("[ " + i + " ] " + " [ " + j + " ] = " + matrix[i][j]);
                } else if (matrix[i][j] > 10) {
                    System.out.println("!!!!!!!");
                }
            }
        }
    }
}

