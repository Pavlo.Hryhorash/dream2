package firstsLessons;

public class lesson5 {
    //    private static final int ARRAY_SIZE = 6;
    private static final int FOOTBALL = 1;
    private static final int CHESS = 2;
    private static final int PING_PONG = 3;

    public static void main(String[] args) {
//        int[] numbers = new int[ARRAY_SIZE];
//        for (int i = 0; i < ARRAY_SIZE; i++) {
//            numbers[i] = i;
//        }
//        for (int i = 0; i < numbers.length; i++) {
//            System.out.println(numbers[i]);


        String playerName1 = "Pavlo";
        int playerAge1 = 30;
        int playerEnergy1 = 20;
        String playerName2 = "Kolia";
        int playerAge2 = 10;
        int playerEnergy2 = 10;
        String playerName3 = "Vasyl";
        int playerAge3 = 20;
        int playerEnergy3 = 15;
        startGame(playerName1, playerAge1, playerEnergy1, FOOTBALL);
        startGame(playerName2, playerAge2, playerEnergy2, FOOTBALL);
        startGame(playerName3, playerAge3, playerEnergy3, FOOTBALL);

    }

    public static void startGame(String name, int age, int energy, int gameAddress) {
        switch (gameAddress) {
            case FOOTBALL:
                football(name, age, energy);
                break;
            case CHESS:
                chess(name, age, energy);
                break;
            case PING_PONG:
                pingPong(name, age, energy);
                break;

        }
    }

    private static void football(String name, int age, int energy) {
        System.out.println("Player -> " + name + " start football");
        if (age > 15 && age < 35) {
            System.out.println("Player can play game!");
        } else {
            System.out.println("Player can't play game, because age not allowed!!!");
            return;
        }
        int[] goals = new int[energy];
        for (int i = 0; i < goals.length; i++){
            int hit = (int)(Math.random()*9);
            if (hit > 3 && hit < 6) {
                goals[i] = 1;
            } else {
                goals[i] = 0;
            }

        }
        for (int i = 0; i < goals.length; i++){
            if (goals[i] == 1){
                System.out.println("Player hited goal!");
            } else {
                System.out.println("Player fall");
            }
        }
    }

    private static void chess(String name, int age, int energy) {
        System.out.println("Player ->" + name + "start chess");
    }

    private static void pingPong(String name, int age, int energy) {
        System.out.println("Player ->" + name + "start ping-pong");
        if (age > 20 && age < 50) {
            System.out.println("Player can play game!");
        } else {
            System.out.println("Player can't play game, because age not allowed!!!");
        }
    }

}

