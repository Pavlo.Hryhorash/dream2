public class lesson3 {
    public static void main(String[] args) {
        int stop = 2;
        String name = "Pavlo";
        System.out.println("loopFor");
        loopFor(stop, name);
        System.out.println("loopWhile");
        loopWhile(stop, name);
        System.out.println("loopDoWhile");
        loopDoWhile(stop, name);

        System.out.println("chooseLoop");
        chooseLoop(2, "lesson3");

    }

    public static void loopFor(int stopLoop, String name) {
        for (int i = 0; i < stopLoop; i++) {
            System.out.println(name);
        }
    }

    public static void loopWhile(int stopLoop, String name) {

        while (stopLoop > 0) {
            System.out.println(name);
            stopLoop--;
        }
    }

    public static void loopDoWhile(int stopLoop, String name) {

        do {
            System.out.println(name);
            stopLoop--;
        } while (stopLoop > 0);
    }

    public static void chooseLoop(int index, String text) {
        switch (index) {
            case 1:
                loopFor(2,text);
                break;
            case 2:
                loopWhile(3, text);
                break;
            case 3:
                loopDoWhile(4, text);
                break;
            default:
                System.out.println("do not have");
        }
    }
}
