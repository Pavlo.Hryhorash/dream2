package button;

public abstract class View {
    protected EventListener eventListener;

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }
    public void click(){
        eventListener.onClick();
    }
}
