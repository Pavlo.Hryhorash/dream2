package button;

public interface EventListener {
    void onClick();
}
