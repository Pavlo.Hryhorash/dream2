package exceptions;

import lesson11Animals.Animal;
import lesson11Animals.Cat;
import lesson11Animals.Dog;

public class ExceptionsApp {
    public static void main(String[] args) {
        Cat cat = null;
        try {
            cat.myNameIs();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Method completes");
        }


//        Animal animal = new Cat();
//        Dog dog = (Dog) animal;
//
//        Cat cat = null;
//        cat.myNameIs();
//
//        int[] numbers = new int[5];
//        int number = numbers[10];
    }
}
