package Lesson15;

import Juice.util.MyCollection;

public class Main {
    public static void main(String[] args) {
        MyCollection<Integer> collection = new MyCollection();
        collection.add(new Integer(12));
        collection.add(new Integer(13));

        System.out.println(collection.get(0));
    }
}
