package lock;

import java.util.Scanner;

public class Main {
    private static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        ElectronicalKey electronicalKey = new ElectronicalKey("1234");
        ElectronicalLock electronicalLock = new ElectronicalLock(electronicalKey);

        System.out.println("Enter code");
        String code = scanner.next();
        electronicalLock.open(code);

    }

    public static void lockManipulation(Lock lock, Key key) {
        lock.unlock();
        lock.open(key);
        lock.lock();
        lock.close(key);
        lock.unlock();
        lock.close(key);
        lock.lock();
    }

    public static void electronicalManipulation(ElectronicalLock lock) {
        lock.unlock();
        lock.getKey();
    }
}
