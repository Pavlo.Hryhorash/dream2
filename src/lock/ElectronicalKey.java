package lock;

public class ElectronicalKey implements Key {
    private String code;

    public ElectronicalKey(String code) {
        this.code = code;
    }

    @Override
    public boolean isExist() {
        return false;
    }

    @Override
    public boolean isCodeAccepted(String code) {
        return this.code.equals(code);
    }
}
