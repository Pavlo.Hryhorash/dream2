package lock;

public interface Key {
    boolean isExist();
    boolean isCodeAccepted(String code);
}
