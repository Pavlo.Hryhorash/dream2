package lock;

public class ElectronicalLock {
    private boolean isLocked;
    private boolean isClosed;

    private ElectronicalKey key;

    public ElectronicalLock(ElectronicalKey key) {
        isClosed = true;
        isLocked = true;

        this.key = key;
    }

    public ElectronicalKey getKey() {
        return key;
    }

    public void lock() {
        isLocked = true;
        System.out.println("Electronical Lock is locked");
    }

    public void unlock() {
        isLocked = false;
        System.out.println("Electronical Lock is unlocked");
    }

    public void open(String code) {
        if (!key.isCodeAccepted(code)) {
            System.out.println("You entered wrong code");
            return;
        }
        if (isClosed && isLocked) {
            System.out.println("You must unlock before opening");
        } else if (!isClosed) {
            System.out.println("Already opened");
        } else if (isClosed && !isLocked) {
            isClosed = false;
            System.out.println("Electronical Lock is opened");
        }
    }

    public void close(Key key) {
        if (isClosed && isLocked || isClosed && !isLocked) {
            System.out.println("Already closed!");
        } else if (!isClosed && isLocked) {
            System.out.println("You must unlock before closing!");
        } else if (!isClosed && !isLocked) {
            isClosed = true;
            System.out.println("Electronical Lock is closed");
        }
    }
}
