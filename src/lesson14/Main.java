package lesson14;

public class Main {
    public static void main(String[] args) {
        Client john = new Client("John", "Smith");
        Client sara = new Client("Sara", "Smith");
        sara.setHasCredit(true);

        Decision.showDecisionForClient(john);
        Decision.showDecisionForClient(sara);

    }
}
