package lesson14;

public class Decision {
    public static Status decide(Client client){
        boolean hasCredit = client.isHasCredit();
        if (!hasCredit) {
            return Status.APPROVE;
        } else {
            return Status.CANCEL;
        }
    }
    public static void showDecisionForClient (Client client){
        Status decisionStatus = decide(client);
        System.out.println("Decision result for client " + client.getFirstName() + " " + client.getLastName() + " = " + decisionStatus);
    }
}
