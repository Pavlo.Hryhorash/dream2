package lesson14;

public class Client {
    private String firstName;
    private String lastName;
    private boolean hasCredit;

    public Client(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
        this.hasCredit = false;
    }
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }

    public boolean isHasCredit() {
        return hasCredit;
    }

    public void setHasCredit(boolean hasCredit) {
        this.hasCredit = hasCredit;
    }

}
