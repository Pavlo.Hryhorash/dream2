package lesson12;

public class Chevrolet extends Car {

    public String bodyType;

    public String wheelType;

    public Chevrolet(String name, String model, int speed) {
        super(name, model, speed);
    }

    public String getBodyType() {
        return bodyType;
    }

    public String getWheelType() {
        return wheelType;
    }
}
