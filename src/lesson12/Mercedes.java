package lesson12;

public class Mercedes extends Car {

    public String spoilerModel;

    public int numbersOfDoors;

    public Mercedes(String name, String model, int speed, String spoilerModel, int numbersOfDoors) {
        super(name, model, speed);
        this.spoilerModel = spoilerModel;
        this.numbersOfDoors = numbersOfDoors;
    }

    public String getSpoilerModel() {
        return spoilerModel;
    }

    public int getNumbersOfDoors() {
        return numbersOfDoors;
    }
}
