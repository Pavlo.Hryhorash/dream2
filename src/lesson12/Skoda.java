package lesson12;

public class Skoda extends Car{

    public String airModel;

    public int spareWheel;

    public Skoda(String name, String model, int speed) {
        super(name, model, speed);
    }

    public String getAirModel() {
        return airModel;
    }

    public int getSpareWheel() {
        return spareWheel;
    }
}
