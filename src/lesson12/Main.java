package lesson12;

public class Main {
    public static void main(String[] args) {

        Car[] cars = new Car[]{
                new Mercedes("Mercedes", "Sprinter", 200, "A", 3),
                new Mercedes("Mercedes", "Vito", 190, "B", 4),
                new Mercedes("Mercedes", "Gle", 220, "C", 2),
                new Chevrolet("Chevrolet", "Camaro", 180),
                new Skoda("Skoda", "Octavia", 150)
        };

        for (int i = 0; i < cars.length; i++) {
            if (cars[i].getName().equals("Skoda")) {
                System.out.println(cars[i].getName());
                System.out.println(cars[i].getModel());
                System.out.println(cars[i].getSpeed());
                System.out.print("\n");
            }
        }
        for (int i = 0; i < cars.length; i++) {
            if (cars[i] instanceof Mercedes) {
                char[] massive = cars[i].name.toCharArray();
                for (int j = 0 ; j < massive.length; j++){
                    if (j == 3) massive[j]='0';
                }
                System.out.println(((Mercedes)cars[i]).getSpoilerModel());
                System.out.println(((Mercedes)cars[i]).getNumbersOfDoors());
            }
        }
    }
}
