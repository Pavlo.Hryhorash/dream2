package lesson16;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        User sara = new User("Sara", "Smith", 25);
        User john = new User("John", "Smith", 26);
        User denis = new User("Denis", "Smith", 27);
        User mia = new User("Mia", "Smith", 28);

        Map<String, User> userMap = new HashMap<>();
        userMap.put("Sara", sara);
        userMap.put("John", john);
        userMap.put("Denis", denis);
        userMap.put("Mia", mia);

        Set<User> users2 = new LinkedHashSet<>();
        users2.add(sara);
        users2.add(sara);
        users2.add(john);
        users2.add(john);
        users2.add(denis);
        users2.add(mia);

        showCollection(users2);

        List<User> family = new ArrayList<>();
        family.add(sara);
        family.add(john);

        List<User> friends = new LinkedList<>();
        friends.add(denis);
        friends.add(mia);



        family.addAll(friends);
//        showCollection(family);
    }

    private static void showCollection(Collection<User> collection) {
        System.out.println("Start...\n");
        for (User user: collection){
            user.showData();
            System.out.println("\n");
        }
        System.out.println("\n");
    }


}
