package lesson16;

public class User implements Comparable<User> {
    private String firstName;
    private String lastName;
    private int age;

    public User(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public int compareTo(User o){
        return new Integer(age).compareTo(new Integer(o.age));
    }

    public void showData (){
        System.out.println("First name " + firstName);
        System.out.println("Last name " + lastName);
        System.out.println("Age " + age);

    }
}
