package family;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private String address;
    private String email;

    public User(String firstName, String lastName, int age, String address, String email){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
        this.email = email;
    }

    public void showFields(){

        System.out.println("First name: " + this.firstName);
        System.out.println("Last name: " + this.lastName);
        System.out.println("age: " + this.age + " years old");
        System.out.println("He/She leaves in " + this.address);
        System.out.println("email: " + this.email);
        System.out.println("----------------------------------");

    }
}
