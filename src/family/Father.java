package family;

public class Father implements Name {
    private String character = "Character";
    protected String skin = "White";
    protected String eyeColor = "Brown";

    public Father(String character) {
        this.character = character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCharacter() {
        return character;
    }

    @Override
    public void showData() {
        System.out.println(character);
    }
}
