package lesson9;

import lesson9.geometry.Triangle;

public class Main {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(2, 5, 7);
        triangle.solvePerimeter();
        triangle.solveSquare();
        triangle.showParameters();
    }
}
