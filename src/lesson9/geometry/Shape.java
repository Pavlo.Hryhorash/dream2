package lesson9.geometry;

public abstract class Shape {
    protected double square;
    protected double perimeter;

    public abstract double solvePerimeter();

    public abstract double solveSquare();

    public void showParameters(){
        if (perimeter == 0 ){
            System.out.println("Perimeter is not solves yet");
        }
        if (perimeter == 0 ){
            System.out.println("Square is not solves yet");
        }
        System.out.println("Perimeter = " + perimeter);
        System.out.println("Square = " + square);
    }
}
