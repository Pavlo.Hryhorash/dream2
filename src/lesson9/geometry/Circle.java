package lesson9.geometry;

public class Circle extends Shape {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double solvePerimeter() {
        super.perimeter = 2 * Math.PI * radius;
        return super.perimeter;
    }

    @Override
    public double solveSquare() {
        super.square = Math.PI * Math.pow(radius, 2);
        return super.square;
    }


}
