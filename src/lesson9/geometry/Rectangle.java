package lesson9.geometry;

public class Rectangle extends Shape {

    private double width;
    private double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double solvePerimeter() {
        super.perimeter = 2 * (width + height);
        return super.perimeter;
    }

    @Override
    public double solveSquare() {
        super.square = width * height;
        return super.square;
    }
}
