package lesson13;

import lesson13.gameEngine.GameLogic;
import lesson13.model.Skin;
import lesson13.model.SkinRole;
import lesson13.model.Weapon;

public class Main {
    public static void main(String[] args) {
        Skin bot = new Skin(100, new Weapon(20, "Shotgun"), "Bot", SkinRole.BOT);
        Skin gamer = new Skin(100, new Weapon(25, "AKA"), "Pavlo", SkinRole.GAMER);

        GameLogic gameLogic = new GameLogic();

        gameLogic.startGame(gamer, bot);
    }
}
