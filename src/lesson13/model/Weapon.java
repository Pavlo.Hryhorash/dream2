package lesson13.model;

public class Weapon {
    private int damage;
    private String WeaponType;

    public Weapon(int damage, String weaponType) {
        this.damage = damage;
        WeaponType = weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public String getWeaponType() {
        return WeaponType;
    }

    public void setWeaponType(String weaponType) {
        WeaponType = weaponType;
    }
}
