package lesson13.model;

public enum SkinRole {
    GAMER,
    ADMIN,
    BOT
}
