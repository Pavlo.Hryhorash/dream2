package lesson13.model;

public class Skin {
    public long health;
    private Weapon weapon;
    private String name;
    private SkinRole skinRole;

    public Skin() {
    }

    public Skin(long health, Weapon weapon, String name, SkinRole skinRole) {
        this.health = health;
        this.weapon = weapon;
        this.name = name;
        this.skinRole = skinRole;
    }

    public long getHealth() {
        return health;
    }

    public void setHealth(long health) {
        this.health = health;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SkinRole getSkinRole() {
        return skinRole;
    }

    public void setSkinRole(SkinRole skinRole) {
        this.skinRole = skinRole;
    }
}

