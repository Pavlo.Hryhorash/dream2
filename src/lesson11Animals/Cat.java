package lesson11Animals;

public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    public Cat() {
    }

    @Override
    public String say() {
        return "Meow";
    }

    @Override
    public void eat() {
        System.out.println("I drink milk");
    }

    @Override
    public void myNameIs(){
        System.out.println("Implementation for cat");
    }
}
