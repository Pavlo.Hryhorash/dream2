package lesson11Animals;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    public Dog() {
    }

    @Override
    public String say() {
        return "Woof";
    }

    @Override
    public void eat() {
        System.out.println("I eat bones");
    }

    @Override
    public void myNameIs(){
        System.out.println("Implementation for dog");
    }
    public void bit(){
        System.out.println(this.name + " likes bit");
    }
}
