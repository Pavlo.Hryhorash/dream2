package lesson11Animals;

public class Main {
    public static void main(String[] args) {
        Animal cat = new Cat("Murka");
        Animal dog = new Dog();
        Animal cat1 = new SiamicCat("Barsik");

        Animal dog1 = new LaykaDog("Layka");
        Animal dog2 = new DalmaticDog("Bobik");

        cat.eat();
        dog.eat();
        cat1.eat();

        cat.myNameIs();
        dog.myNameIs();
        cat1.myNameIs();

        dog1.myNameIs();
        dog2.myNameIs();


        Animal[] animals = new Animal[2];
        animals[0] = cat;
        animals[1] = dog;

        for (int i = 0; i < animals.length; i++){
            animals[i].myNameIs();
            animals[i].eat();

            System.out.println(animals[i].say());
        }

    }
}
